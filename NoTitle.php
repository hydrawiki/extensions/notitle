<?php
/*
 *  NoTitle
 *  Adds a magic word that hides the main title heading in a page
 *
 * @file NoTitle.php
 * @author Carlo Cabanilla
 * @author Tony Boyles
 */

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'NoTitle' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['NoTitle'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for NoTitle extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
 } else {
	die( 'This version of the NoTitle extension requires MediaWiki 1.25+' );
}
