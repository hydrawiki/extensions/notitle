<?php
/*
 *  NoTitle
 *  Adds a magic word that hides the main title heading in a page
 *
 * @file NoTitle.body.php
 * @author Tony Boyles
 */

class NoTitleHooks {

	/**
	 * Add magic word to list of doubleUnderscoreIDs.
	 *
	 * @access	public
 	 * @param	array &$doubleUnderscoreIDs
 	 * @return	void
	 */
	public static function onGetDoubleUnderscoreIDs( &$doubleUnderscoreIDs ) {
		$doubleUnderscoreIDs[] = 'MAG_NOTITLE';
	}

	/**
	 * Check for output property and if it exists add relevant styling.
	 *
	 * @access	public
	 * @param	object	Parser
	 * @param	string	Article Text
	 * @return	void
	 */
	public static function onParserBeforeTidy(&$parser, &$text) {
		if ($parser->getOutput()->getProperty('MAG_NOTITLE') !== false) {
			$parser->getOutput()->addModuleStyles(['ext.notitle.css']);
		}
		return true;
	}
}
